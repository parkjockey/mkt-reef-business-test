package data_containers;

public class GeneralData {
    public static String envName;
    public static String reefUsername;
    public static String reefPassword;
    public static String reefWebApp;
    public static String apiAccessToken;
    public static String identityAccessApiBaseUrl;
    public static Integer testcaseId;
    public static Integer runId;
    public static String browser;
    public static String requestContentType;
    public static String reefOrganization;
}
