package common.helpers.ui;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.util.HashMap;
import java.util.Map;

import static java.util.concurrent.TimeUnit.SECONDS;

public class BaseDriver {

    public static RemoteWebDriver driver;
    private final static int IMPLICIT_WAIT = 10;

    public static void initializeDriver(String browser) throws Exception {
        switch (browser) {
            case "headlessChrome":
                WebDriverManager.chromedriver().setup();
                driver = new ChromeDriver(getChromeOptions(true));
                driver.manage().timeouts().implicitlyWait(IMPLICIT_WAIT, SECONDS);
                break;

            case "headlessFirefox":
                WebDriverManager.firefoxdriver().setup();
                driver = new FirefoxDriver(getFirefoxOptions(true));
                driver.manage().timeouts().implicitlyWait(IMPLICIT_WAIT, SECONDS);
                break;

            case "chrome":
                WebDriverManager.chromedriver().setup();
                driver = new ChromeDriver(getChromeOptions(false));
                driver.manage().timeouts().implicitlyWait(IMPLICIT_WAIT, SECONDS);
                break;

            case "firefox":
                WebDriverManager.firefoxdriver().setup();
                driver = new FirefoxDriver(getFirefoxOptions(false));
                driver.manage().timeouts().implicitlyWait(IMPLICIT_WAIT, SECONDS);
                break;

            default:
                throw new Exception("Browser Name" + browser + "didn't match the expected Browsers");
        }

    }


    private static ChromeOptions getChromeOptions(boolean headless) {
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.setHeadless(headless);
        chromeOptions.addArguments("start-maximized");
        chromeOptions.addArguments("--disable-extensions");
        chromeOptions.addArguments("--disable-infobars");
        chromeOptions.addArguments("--ignore-certificate-errors");
        chromeOptions.addArguments("test-type");
        chromeOptions.addArguments("--enable-precise-memory-info");
        chromeOptions.addArguments("--disable-popup-blocking");
        chromeOptions.addArguments("--disable-default-apps");
        Map<String, Object> prefs = new HashMap<>();
        prefs.put("credentials_enable_service", false);
        prefs.put("profile.password_manager_enabled", false);
        chromeOptions.setExperimentalOption("prefs", prefs);
        chromeOptions.setExperimentalOption("excludeSwitches", new String[]{"enable-automation"});
        return chromeOptions;
    }

    private static FirefoxOptions getFirefoxOptions(boolean headless) {
        FirefoxOptions firefoxOptions = new FirefoxOptions();
        firefoxOptions.addArguments("start-maximized");
        firefoxOptions.addArguments("--disable-extensions");
        firefoxOptions.addArguments("--ignore-certificate-errors");
        firefoxOptions.addArguments("--disable-infobars");
        firefoxOptions.addArguments("--disable-gpu");
        firefoxOptions.setAcceptInsecureCerts(headless);
        return firefoxOptions;
    }


}


