package common.helpers.ui;

import io.qameta.allure.Allure;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.util.function.Function;

public class PageObjectBase extends BaseDriver {
    public static WebDriverWait wait;
    private static String screenshotPath = "build/allure-results/";
    private static JavascriptExecutor js = (JavascriptExecutor) driver;

    public static void takeScreenshot(String scenarioName) throws IOException {
        File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(scrFile, new File(screenshotPath + scenarioName + ".png"));
    }


    public static void addScreenshotAllure(String scenarioName) throws IOException {
        Path content = Paths.get(screenshotPath + scenarioName + ".png");
        try (InputStream is = Files.newInputStream(content)) {
            Allure.addAttachment(scenarioName, is);
        }
    }


    public static WebElement waitToBeClickable(WebElement element) {
        wait = new WebDriverWait(driver, 15);
        return wait.until(ExpectedConditions.elementToBeClickable(element));
    }


    public static WebElement retryFindElement(By by) {
        WebElement el = null;
        int attempts = 0;
        while (attempts < 3) {
            try {
                el = waitToBeClickable(driver.findElement(by));
                if (el != null) {
                    break;
                }
            } catch (org.openqa.selenium.StaleElementReferenceException e) {
                el = waitToBeClickable(driver.findElement(by));
            }
            attempts++;
        }
        return el;
    }


    public static void performClick(WebElement element) {
        WebElement ele = element;
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", ele);
    }


    public static void zoomTo(String zoomLevel) throws InterruptedException {
        JavascriptExecutor executor = (JavascriptExecutor) BaseDriver.driver;
        executor.executeScript("document.body.style.zoom = '" + zoomLevel + "'");
        Thread.sleep(5000);
    }


    public static void resetZoom() throws InterruptedException {
        JavascriptExecutor executor = (JavascriptExecutor) BaseDriver.driver;
        executor.executeScript("document.body.style.zoom = '1'");
        Thread.sleep(5000);
    }

    public static void scrollDownToElement(WebElement element) {
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
    }

    public static void scrollUpToElement(WebElement element) {
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(false);", element);
    }

    public static WebElement fluentWait(By elementLocator) {
        Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                .withTimeout(Duration.ofSeconds(20))
                .pollingEvery(Duration.ofSeconds(2))
                .ignoring(NoSuchElementException.class)
                .ignoring(ElementNotInteractableException.class)
                .ignoring(ElementNotSelectableException.class)
                .ignoring(ElementNotVisibleException.class)
                .ignoring(ElementClickInterceptedException.class);

        WebElement element = wait.until(new Function<WebDriver, WebElement>() {
            public WebElement apply(WebDriver driver) {
                return driver.findElement(elementLocator);
            }
        });
        return element;
    }

    public static WebElement fluentWait(WebElement element) {
        Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                .withTimeout(Duration.ofSeconds(20))
                .pollingEvery(Duration.ofSeconds(2))
                .ignoring(NoSuchElementException.class)
                .ignoring(ElementNotInteractableException.class)
                .ignoring(ElementNotSelectableException.class)
                .ignoring(ElementNotVisibleException.class)
                .ignoring(ElementClickInterceptedException.class);

        WebElement webElement = wait.until(new Function<WebDriver, WebElement>() {
            public WebElement apply(WebDriver driver) {
                return element;
            }
        });
        return webElement;
    }

    public static void pageRefresh() {
        driver.navigate().refresh();
    }

    public static void wait(int timeInMilliSeconds) {
        var endTime = System.currentTimeMillis() + timeInMilliSeconds;
        var startTime = System.currentTimeMillis();
        while (endTime > startTime) {
            startTime = System.currentTimeMillis();
        }
    }

    public static void selectDropdownValue(WebElement dropdownElement, WebElement selectValueElement) {
        fluentWait(dropdownElement);
        dropdownElement.click();
        wait(500);
        fluentWait(selectValueElement);
        selectValueElement.click();
    }

}
