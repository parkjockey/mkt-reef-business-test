package common.helpers.rest;

import io.restassured.response.ExtractableResponse;
import io.restassured.response.Response;

import java.util.Map;

import static io.restassured.RestAssured.given;

public class GenerateResponse {

    public ExtractableResponse<Response> postRequest(String endpoint, String requestBody) {
        return
                given()
                        .urlEncodingEnabled(false)
                        .header("Content-Type", "application/json")
                        .body(requestBody)
                        .when()
                        .post(endpoint)
                        .then()
                        .statusCode(200)
                        .extract();
    }

    public ExtractableResponse<Response> postRequest(String endpoint, Map<String, String> headers, String requestBody) {
        return
                given()
                        .urlEncodingEnabled(false)
                        .headers(headers)
                        .body(requestBody)
                        .when()
                        .post(endpoint)
                        .then()
                        .statusCode(201)
                        .extract();
    }

    public ExtractableResponse<Response> putRequest(String endpoint, Map<String, String> headers, String requestBody) {
        return
                given()
                        .urlEncodingEnabled(false)
                        .headers(headers)
                        .body(requestBody)
                        .when()
                        .put(endpoint)
                        .then()
                        .statusCode(200)
                        .extract();
    }

    public void deleteRequest(String endpoint, Map<String, String> headers) {
        given()
                .urlEncodingEnabled(false)
                .headers(headers)
                .delete(endpoint)
                .then()
                .statusCode(200);
    }


    public ExtractableResponse<Response> getRequest(String endpoint, Map<String, String> headers) {
        return
                given()
                        .urlEncodingEnabled(false)
                        .headers(headers)
                        .when()
                        .get(endpoint)
                        .then()
                        .extract();
    }
}
