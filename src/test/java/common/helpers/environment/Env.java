package common.helpers.environment;

public class Env {

    public String name;
    public String reefWebApp;
    public String storefrontBaseUri;
    public String identityAccessBaseUri;
    public String requestContentType;
    public String reefOrganization;
}
