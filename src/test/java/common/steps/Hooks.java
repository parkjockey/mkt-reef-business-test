package common.steps;

import common.helpers.testrail.TestRailHelpers;
import data_containers.GeneralData;
import io.cucumber.java.After;
import io.cucumber.java.Scenario;


public class Hooks {

    @After
    public void afterTest(Scenario scenario) {
        TestRailHelpers.updateRun(GeneralData.testcaseId);
        if (scenario.isFailed() && (GeneralData.testcaseId != null)) {
            TestRailHelpers.addStatusForCase(5, GeneralData.testcaseId, "Test failed");
        } else {
            if (GeneralData.testcaseId != null) {
                TestRailHelpers.addStatusForCase(1, GeneralData.testcaseId, "Test passed");
            }
        }
    }


}

