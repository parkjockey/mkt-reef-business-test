package runners;

import common.helpers.environment.Env;
import common.helpers.environment.YamlReader;
import common.helpers.testrail.TestRailHelpers;
import common.helpers.ui.BaseDriver;
import data_containers.GeneralData;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ui.page_object.LoginPage;

@RunWith(Cucumber.class)

@CucumberOptions(
        publish = true,
        features = {
                "src/test/resources/features/backend",
                "src/test/resources/features/frontend"
        },
        glue = {
                "stepdefs"
        }
//        , tags = "@backend"

        , plugin = {"com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:", "json:target/cucumber-report.json"}


)
public class CucumberRunner {

    @BeforeClass
    public static void beforeTestSuiteActions() throws Exception {
        Logger logger = LoggerFactory.getLogger(CucumberRunner.class);
        logger.info("Test Started");

        //Fetch Environment Variables
        {
            GeneralData.envName = System.getenv("env");
            if (GeneralData.envName == null || GeneralData.envName.isEmpty()) {
                throw new Exception("Environment is NULL");
            }

            GeneralData.reefUsername = System.getenv("reefUsername");
            if (GeneralData.reefUsername == null || GeneralData.reefUsername.isEmpty()) {
                throw new Exception("Reef Username is NULL");
            }

            GeneralData.reefPassword = System.getenv("reefPassword");
            if (GeneralData.reefPassword == null || GeneralData.reefPassword.isEmpty()) {
                throw new Exception("Reef Password is NULL");
            }

            GeneralData.browser = System.getenv("browser");
            if (GeneralData.browser == null || GeneralData.browser.isEmpty()) {
                throw new Exception("Browser is NULL");
            }
        }


        //Read Yaml File
        {
            Env testEnvironment = YamlReader.getEnvironmentFromYaml(GeneralData.envName);
            GeneralData.reefWebApp = testEnvironment.reefWebApp;
            GeneralData.identityAccessApiBaseUrl = testEnvironment.identityAccessBaseUri;
            GeneralData.requestContentType = testEnvironment.requestContentType;
            GeneralData.reefOrganization = testEnvironment.reefOrganization;
        }

        //Initialize Web driver
        {
            BaseDriver.initializeDriver(GeneralData.browser);
            BaseDriver.driver.get(GeneralData.reefWebApp);
            LoginPage.loginToMicrosoftAccount(GeneralData.reefUsername, GeneralData.reefPassword);
        }

        //Create Test Rail run
        {
            TestRailHelpers.createTestRailInstance(GeneralData.reefUsername, GeneralData.reefPassword);
            TestRailHelpers.setProjectSuiteId("REEF Cloud -  Storefront Admin", "Storefront");
            TestRailHelpers.createRun();
        }


    }

    @AfterClass
    public static void afterTestSuiteActions() {
        TestRailHelpers.closeRun();
        BaseDriver.driver.quit();
    }

}
