## Introduction

Test automation framework for Business Portal for testing the Web UI.
* frontend repo - https://bitbucket.org/parkjockey/co-mkt-reef-business-frontend.git



## Prerequisites
* gradle 6.0.1
* Open JDK 13
* IntelliJ IDEA - with lombok, cucumber plugins (in settings/gradle set gradle JVM to java 13)

## Clone a repository
https://bitbucket.org/parkjockey/mkt-reef-business-test.git

### Environmental Variables

These values must be set as environmental variables if running the test locally or while running in pipelines

| Name                    | Example Value 	       |
| ----------------------- | -----------------------|
| env     				  | dev or qa  			   |
| reefUsername 			  | Reef Username      	   |
| reefPassword         	  | Reef Username          |
| browser         		  | chrome, headlesschrome |



## Dependencies
* For running tests - JUnit runner
* For BDD - cucumber(core, java13, JVM, JUnit, gherkin), groovy-all
* For sending rest requests - rest-assured 
* For preparing the body and asserting response - Gson
* For preparing graphQl - test-graphql-java
* For UI - selenium and webdrivermanager
* For access DB - jdbc
* For Kafka integration - kafka-clients and other
* Replacement getters and setter - Lombok
* Accessing yaml files - snake-yaml
* Logging - slf4j
* Reporting - allure (cucumber4-jvm, rest-assured), testrail-api-java-client, cukedoctor

## Project structure:
   * src/test/java/common - will be used for storing general helpers in common packages 
   * src/test/java/data_containers - will use for PicoContainer DI
   * src/test/java/rest/your_microservice - will be used for storing steps, requests, responses and helpers for this microservice.
   * src/test/ui - will be used for storing selinum E2E tests
   * src/test/java/runners - will store runner class
   * src/test/resources -  will store features in the features package. Then feature package will have packages related to each microservice with specific features files. Also,  stores yaml for different environments (URL, credentials etc…)

## How to run tests: 
  * By Junit runner - just run class CucumberRunner in runners package. Default it will run tests on dev environment.
  * By terminal type command: 
  gradle cucumber -Denv={envName} -Ddriver={driverName} -Dcucumber.options="-t @tag1,@tag2 -t ~@igonredTag"  
    
  Note: envName is stored in propeties.yml file.
  If you have some tests that are currently failing because they are waiting future implementation, annotate them with @failing so we can ignore them during our run.
  
## Reporting:   
  * Extent Report is generated after every run and stored in test-output folder.
  * If you want to generate Allure report type in terminal: allure generate <yourPathToAllureReport/allure-results> --clear. 
  After the task is finished check the allure-report package and open with the preferred browser open index.html file. 
  (If you run as Junit run Class it allure-result will be generated in the root package if it is run via terminal/Gradle 
  run it is stored in root/build/allure-result)
  * Adoc file is generated at the end of run
  * TestRail run report is also generated in every run
  
